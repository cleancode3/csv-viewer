package com.schulung.csvviewer.tableviewer;

import com.schulung.csvviewer.tablecreator.Table;
import com.schulung.csvviewer.tablecreator.Tabulator;
import com.schulung.csvviewer.tablenavigation.CommandInterpreter;
import com.schulung.csvviewer.tablenavigation.PagedTable;
import com.schulung.csvviewer.tablenavigation.TableNavigationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TableVisualizerTest {

  private static final String testData = "Name;Strasse;Ort;Alter\n"
      + "Peter Pan;Am Hang 5;12345 Einsam;42\n"
      + "Maria Schmitz;Kölner Straße 45;50123 Köln;43\n"
      + "Paul Meier;Münchener Weg 1;87654 München;65\n"
      + "Paul Ott;OtterStraße 174;75174 Pforzheim;57";

  private static final Tabulator tabulator = new Tabulator();
  private static final TableVisualizer visualizer = new TableVisualizer();
  private static final TableNavigationService navigationService = new TableNavigationService();
  private static PagedTable pagedTable;

  @BeforeAll
  static void setUp() {
    Table table = tabulator.createTable(testData);
    pagedTable= navigationService.buildPages(2, table.getHeaders(), table.getContent());
  }

  @Test
  void displayValidDataTableOk() {
    var table = tabulator.createTable(testData);
    var tableString= visualizer.display(table);
    var expectedOutput = "Name         |Strasse         |Ort            |Alter|\n"
        + "-------------+----------------+---------------+-----+\n"
        + "Peter Pan    |Am Hang 5       |12345 Einsam   |42   |\n"
        + "Maria Schmitz|Kölner Straße 45|50123 Köln     |43   |\n"
        + "Paul Meier   |Münchener Weg 1 |87654 München  |65   |\n"
        + "Paul Ott     |OtterStraße 174 |75174 Pforzheim|57   |\n";

    Assertions.assertEquals(expectedOutput, tableString);
  }

  @Test
  void displayCommandFirstPageTableOk() {
    var expectedFirstPage = "Name         |Strasse         |Ort            |Alter|\n"
        + "-------------+----------------+---------------+-----+\n"
        + "Peter Pan    |Am Hang 5       |12345 Einsam   |42   |\n"
        + "Maria Schmitz|Kölner Straße 45|50123 Köln     |43   |\n";

    displayTable(1, "f", expectedFirstPage);
  }

  @Test
  void displayCommandLastPageTableOk() {
    var expectedLastPage = "Name         |Strasse         |Ort            |Alter|\n"
        + "-------------+----------------+---------------+-----+\n"
        + "Paul Meier   |Münchener Weg 1 |87654 München  |65   |\n"
        + "Paul Ott     |OtterStraße 174 |75174 Pforzheim|57   |\n";

    displayTable(1, "l", expectedLastPage);
  }

  @Test
  void displayFirstPageNavigationNextPageTableOk() {
    var expectedNextPage = "Name         |Strasse         |Ort            |Alter|\n"
        + "-------------+----------------+---------------+-----+\n"
        + "Paul Meier   |Münchener Weg 1 |87654 München  |65   |\n"
        + "Paul Ott     |OtterStraße 174 |75174 Pforzheim|57   |\n";

    displayTable(1, "n", expectedNextPage);
  }

  private void displayTable(final int currentPage, final String command, final String expectedPage) {
    var commandInterpreter = new CommandInterpreter(currentPage);
    var firstPage = commandInterpreter.interpret(command, pagedTable);

    var page = visualizer.display(firstPage);

    Assertions.assertEquals(expectedPage, page);
  }
}
