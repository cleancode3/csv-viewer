package com.schulung.csvviewer.tablecreator;

import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TabulatorTests {

  private static String testData = "Name;Strasse;Ort;Alter\n"
      + "Peter Pan;Am Hang 5;12345 Einsam;42\n"
      + "Maria Schmitz;Kölner Straße 45;50123 Köln;43\n"
      + "Paul Meier;Münchener Weg 1;87654 München;65\n"
      + "Paul Ott;OtterStraße 174;75174 Pforzheim;57";

  private static final TableHeader headerName = new TableHeader("Name");
  private static final TableHeader headerStrasse = new TableHeader("Strasse");
  private static final TableHeader headerOrt = new TableHeader("Ort");
  private static final TableHeader headerAlter = new TableHeader("Alter");

  @BeforeAll
  static void setUp() {
    headerName.setColumnWidth(13);
    headerStrasse.setColumnWidth(16);
    headerOrt.setColumnWidth(15);
    headerAlter.setColumnWidth(5);
  }

  @Test
  void createTableValidDataHeaderOk() {
    var tabulator = new Tabulator();
    var expectedHeaders = List.of(headerName, headerStrasse, headerOrt, headerAlter);
    var table = tabulator.createTable(testData);
    Assertions.assertNotNull(table.getHeaders());
    Assertions.assertEquals(expectedHeaders.size(), table.getHeaders().size());
    var diff = expectedHeaders.stream()
        .filter(element -> !table.getHeaders().stream().anyMatch(header -> header.getValue().equals(element.getValue())))
        .collect(Collectors.toList());
    Assertions.assertEquals(0, diff.size());
  }

  @Test
  void createTableValidDataContentOk() {
    var tabulator = new Tabulator();
    var table = tabulator.createTable(testData);
    var column1 = new TableContentColumn("Peter Pan", headerName.getValue());
    var column2 = new TableContentColumn("Am Hang 5", headerStrasse.getValue());
    var column3 = new TableContentColumn("12345 Einsam", headerOrt.getValue());
    var column4 = new TableContentColumn("42", headerAlter.getValue());
    column1.setColumnWidth(headerName.getColumnWidth());
    column2.setColumnWidth(headerStrasse.getColumnWidth());
    column3.setColumnWidth(headerOrt.getColumnWidth());
    column4.setColumnWidth(headerAlter.getColumnWidth());
    Assertions.assertNotNull(table.getContent());
    var expectedContent = List.of(column1, column2, column3, column4);
    Assertions.assertEquals(4, table.getContent().size());
    var re = table.getContent().values().stream().filter(columns -> columns.stream().anyMatch(column -> expectedContent.stream().anyMatch(el -> el.getValue().equals(column.getValue())))).collect(
        Collectors.toList());
    Assertions.assertEquals(1, re.size());
  }
}
