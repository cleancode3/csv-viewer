package com.schulung.csvviewer.tablenavigation;

import com.schulung.csvviewer.tablecreator.TableHeader;
import com.schulung.csvviewer.tablecreator.Tabulator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TableNavigationServiceTest {

  private static String testData = "Name;Strasse;Ort;Alter\n"
      + "Peter Pan;Am Hang 5;12345 Einsam;42\n"
      + "Maria Schmitz;Kölner Straße 45;50123 Köln;43\n"
      + "Paul Meier;Münchener Weg 1;87654 München;65\n"
      + "Paul Ott;OtterStraße 174;75174 Pforzheim;57";

  @Test
  void buildPagesValidDataPaginationOk() {
    var tabulator = new Tabulator();
    var table = tabulator.createTable(testData);
    var navigationService = new TableNavigationService();

    var  pagedTable= navigationService.buildPages(3, table.getHeaders(), table.getContent());

    Assertions.assertNotNull(pagedTable);
    Assertions.assertEquals(2, pagedTable.getPageCount());
  }
}
