package com.schulung.csvviewer.tablenavigation;

import com.schulung.csvviewer.tablecreator.TableContentColumn;
import com.schulung.csvviewer.tablecreator.TableHeader;
import java.util.List;
import java.util.Map;

public class PagedTable {

  private final int pageSize;
  private int pageCount;
  private final List<TableHeader> headers;
  private Map<Integer, Map<Integer, List<TableContentColumn>>> content;

  public PagedTable(final int pageSize, List<TableHeader> headers) {
    this.pageSize= pageSize;
    this.headers = headers;
  }

  public void setPageCount(int pageCount) {
    this.pageCount = pageCount;
  }

  public void setContent(
      Map<Integer, Map<Integer, List<TableContentColumn>>> content) {
    this.content = content;
  }

  public int getPageCount() {
    return pageCount;
  }

  public List<TableHeader> getHeaders() {
    return headers;
  }

  public Map<Integer, Map<Integer, List<TableContentColumn>>> getContent() {
    return content;
  }
}
