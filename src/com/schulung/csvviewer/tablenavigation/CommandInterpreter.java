package com.schulung.csvviewer.tablenavigation;

import com.schulung.csvviewer.tablecreator.Table;

public class CommandInterpreter {

  private int currentPage;

  public CommandInterpreter(final int currentPage) {
    this.currentPage = currentPage;
  }

  public Table interpret(final String option, final PagedTable pagedTable) {
    switch (option.toUpperCase()) {
      case "F":
        return firstPage(pagedTable);
      case "P":
        return previousPage(pagedTable);
      case "N":
        return nextPage(pagedTable);
      case "L":
        return lastPage(pagedTable);
      default:
        return null;
    }
  }

  private Table firstPage(final PagedTable pagedTable) {
    currentPage = 1;
    return prepareTable(pagedTable);
  }

  private Table lastPage(final PagedTable pagedTable) {
    currentPage = pagedTable.getPageCount();
    return prepareTable(pagedTable);
  }

  private Table nextPage(final PagedTable pagedTable) {
    currentPage = currentPage +1;
    return prepareTable(pagedTable);
  }

  private Table previousPage(final PagedTable pagedTable) {
    currentPage = currentPage -1;
    return prepareTable(pagedTable);
  }

  private Table prepareTable(final PagedTable pagedTable) {
    var table = new Table(pagedTable.getHeaders());
    table.setContent(pagedTable.getContent().get(currentPage));
    return table;
  }
}
