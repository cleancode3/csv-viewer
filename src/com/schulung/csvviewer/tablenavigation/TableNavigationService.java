package com.schulung.csvviewer.tablenavigation;

import com.schulung.csvviewer.tablecreator.TableContentColumn;
import com.schulung.csvviewer.tablecreator.TableHeader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableNavigationService {

  public PagedTable buildPages(final int pageSize, final List<TableHeader> headers, final Map<Integer, List<TableContentColumn>> content) {
    var pagedTable = new PagedTable(pageSize, headers);
    var pageCount = content.size()%pageSize != 0 ? (content.size()/pageSize + 1) : content.size()/pageSize;
    pagedTable.setPageCount(pageCount);
    pagedTable.setContent(buildBlock(content, pageSize, pageCount));
    return pagedTable;
  }

  private Map<Integer, Map<Integer, List<TableContentColumn>>> buildBlock(final Map<Integer, List<TableContentColumn>> content, final int blockSize, final int pageCount) {
    var blocks = new HashMap<Integer, Map<Integer, List<TableContentColumn>>>();
    int index = 1;
    for(int  i=1; i <= pageCount; i++) {
      var temp = new HashMap<Integer, List<TableContentColumn>>();
      for (; (index <= blockSize*i)&&(!content.isEmpty()); index++) {
        temp.put(index, content.remove(index));
      }
      blocks.put(i, temp);
    }
    return blocks;
  }
}
