package com.schulung.csvviewer.tablenavigation;

public class Menu {

  private final String[] options = {
      "F)irst page",
      "P)revious page",
      "N)ext page",
      "L)ast page",
      "E)xit"
  };

  public void printMenu() {
    for (String option : options){
      System.out.println(option);
    }
    System.out.print("Choose your option : ");
  }

}
