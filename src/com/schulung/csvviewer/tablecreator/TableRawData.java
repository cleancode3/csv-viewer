package com.schulung.csvviewer.tablecreator;

import java.util.List;

public class TableRawData {

  private final String header;
  private final List<String> tableContent;

  public TableRawData(String header, List<String> tableContent){
    this.header = header;
    this.tableContent= tableContent;
  }

  public String getHeader() {
    return header;
  }

  public List<String> getTableContent() {
    return tableContent;
  }
}
