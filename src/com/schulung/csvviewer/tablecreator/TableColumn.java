package com.schulung.csvviewer.tablecreator;

public class TableColumn {
  private final String value;
  private int columnWidth;

  public TableColumn(final String value) {
    this.value = value;
    this.columnWidth = value.length();
  }

  public void setColumnWidth(int columnWidth) {
    this.columnWidth = columnWidth;
  }

  public int getColumnWidth() {
    return columnWidth;
  }

  public String getValue() {
    return value;
  }
}
