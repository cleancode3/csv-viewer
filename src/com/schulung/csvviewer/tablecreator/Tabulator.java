package com.schulung.csvviewer.tablecreator;

public class Tabulator implements ITabulator {

  private final TabulatorService tabulatorService = new TabulatorService();

  @Override
  public Table createTable(String rawData) {
    var tableRawData = tabulatorService.extractAllLinesFromRawData(rawData);
    var headers = tabulatorService.createTableHeaders(tableRawData.getHeader());
    return tabulatorService.parseContentToTable(headers, tableRawData.getTableContent());
  }
}
