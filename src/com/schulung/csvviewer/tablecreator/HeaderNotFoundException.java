package com.schulung.csvviewer.tablecreator;

public class HeaderNotFoundException extends RuntimeException {
  public HeaderNotFoundException(final String headerName) {
    super("Header could not be found " + headerName);
  }
}