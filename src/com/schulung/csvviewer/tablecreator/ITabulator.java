package com.schulung.csvviewer.tablecreator;

public interface ITabulator {
  Table createTable(final String rawData);
}
