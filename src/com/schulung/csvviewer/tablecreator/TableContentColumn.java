package com.schulung.csvviewer.tablecreator;

public class TableContentColumn extends TableColumn {

  private final String headerName;

  public TableContentColumn(final String value, final String headerName) {
    super(value);
    this.headerName = headerName;
  }
  public String getHeaderName() { return headerName; }
}
