package com.schulung.csvviewer.tablecreator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TabulatorService {

  public TableRawData extractAllLinesFromRawData(final String rawData){
    var allLines = rawData.split("\n");
    return new TableRawData(allLines[0], Arrays.asList(Arrays.copyOfRange(allLines, 1, allLines.length)));
  }

  public Table parseContentToTable(final List<TableHeader>heads, final List<String> lines) {
    var table = new Table(heads);

    //TODO: extract
    lines.forEach(line -> {
      var tableColumns = convertRawLineToTableColumns(line);
      if (isRowValid(heads.size(), tableColumns.length)) {
        buildTableRow(table, heads, tableColumns);
      }
    });
    updateTableColumnWidth(table);
    return table;
  }

  public List<TableHeader> createTableHeaders(final String line) {
    var headers= Arrays.asList(line.split(";"));
    return headers.stream().map(TableHeader::new).collect(Collectors.toList());
  }

  private static String[] convertRawLineToTableColumns(final String line) {
    return Arrays.stream(line.split(";")).filter(str -> !str.isEmpty())
        .toArray(String[]::new);
  }

  private static void buildTableRow(final Table table, final List<TableHeader> heads, final String[] tableColumns) {
    var columns = new ArrayList<TableContentColumn>();
    for (int index =0; index < tableColumns.length; index++) {
      var header = heads.get(index);
      var tableColumnValue= tableColumns[index];
      columns.add(new TableContentColumn(tableColumnValue, header.getValue()));
      if (header.getColumnWidth() < tableColumnValue.length()) {
        header.setColumnWidth(tableColumnValue.length());
      }
    }
    table.addRow(columns);
  }

  //TODO needed?
  private static boolean isRowValid(final int headerSize,  final int tableRowLength) {
    return headerSize == tableRowLength;
  }

  private void updateTableColumnWidth(final Table table) {
    table.getContent().forEach((rowIndex, row) -> row.forEach(column -> column.setColumnWidth(getColumnWidthByHeaderName(table.getHeaders(),column.getHeaderName()))));
  }

  private int getColumnWidthByHeaderName(final List<TableHeader> headers, final String headerName) {
    return headers.stream().filter(header -> Objects.equals(header.getValue(), headerName)).findFirst().orElseThrow(() -> new HeaderNotFoundException(headerName)).getColumnWidth();
  }
}
