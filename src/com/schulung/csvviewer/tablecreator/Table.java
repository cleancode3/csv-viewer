package com.schulung.csvviewer.tablecreator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Table {
  private final List<TableHeader> headers;
  private Map<Integer, List<TableContentColumn>> content;

  public Table(final List<TableHeader> headers) {
    this.headers = headers;
    this.content = new HashMap<>();
  }

  public void setContent(Map<Integer, List<TableContentColumn>> content) {
    this.content = content;
  }

  public Map<Integer, List<TableContentColumn>> getContent() { return content; }

  public List<TableHeader> getHeaders() {
    return headers;
  }

  public void addRow(final List<TableContentColumn> columnValues) {
    content.put(content.size() +1, columnValues);
  }
}
