package com.schulung.csvviewer;

import com.schulung.csvviewer.tablecreator.Tabulator;
import com.schulung.csvviewer.tablenavigation.CommandInterpreter;
import com.schulung.csvviewer.tablenavigation.Menu;
import com.schulung.csvviewer.tablenavigation.TableNavigationService;
import com.schulung.csvviewer.tableviewer.TableVisualizer;
import java.util.Scanner;

public class Main {

    private static String testData = "Name;Strasse;Ort;Alter\n"
        + "Peter Pan;Am Hang 5;12345 Einsam;42\n"
        + "Maria Schmitz;Kölner Straße 45;50123 Köln;43\n"
        + "Paul Meier;Münchener Weg 1;87654 München;65\n"
        + "Paul Ott;OtterStraße 174;75174 Pforzheim;57";

    public static void main(String[] args) {
        int pageSize = 2;
        int currentPage = 1;
        String option = "F";

        var tabulator = new Tabulator();
        var table = tabulator.createTable(testData);

        var tableNavigator = new TableNavigationService();
        var pagedTable= tableNavigator.buildPages(pageSize, table.getHeaders(), table.getContent());

        var menu = new Menu();
        var commandInterpreter = new CommandInterpreter(currentPage);

        Scanner scanner = new Scanner(System.in);

        while (!"E".equalsIgnoreCase(option)) {
            menu.printMenu();
            try {
                option = scanner.next();
                var tableVisualizer = new TableVisualizer();
                var tableToDisplay= commandInterpreter.interpret(option, pagedTable);
                if(tableToDisplay != null) {
                    tableVisualizer.display(tableToDisplay);
                }
            } catch (Exception ex) {
                System.out.println("Error. Please Select again");
                scanner.next();
            }
        }
    }
}
