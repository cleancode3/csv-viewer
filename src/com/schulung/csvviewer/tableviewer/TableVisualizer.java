package com.schulung.csvviewer.tableviewer;

import com.schulung.csvviewer.tablecreator.Table;
import com.schulung.csvviewer.tablecreator.TableContentColumn;
import com.schulung.csvviewer.tablecreator.TableHeader;
import java.util.List;
import java.util.Map;

public class TableVisualizer implements IPrintTable {

  @Override
  public String display(final Table table) {
    var tableString = new StringBuilder();
    tableString.append(printHeader(table.getHeaders()));
    tableString.append(printLineSeparator(table.getHeaders()));
    tableString.append(printBody(table.getContent()));
    print(tableString.toString());
    return tableString.toString();
  }

  private String printHeader(final List<TableHeader> headers) {
    StringBuilder rowString = new StringBuilder();
    headers.forEach(header -> rowString.append(buildColumn(header.getValue(), header.getColumnWidth(), " ", "|")));
    rowString.append("\n");
    return rowString.toString();
  }

  private String printBody(final Map<Integer, List<TableContentColumn>> bodyContent) {
    StringBuilder rowString = new StringBuilder();
    bodyContent.forEach((rowIndex, row) -> {
        row.forEach(tableColumn -> rowString.append(buildColumn(tableColumn.getValue(), tableColumn.getColumnWidth(), " ", "|")));
        rowString.append("\n");
    });
    return rowString.toString();
  }

  private String printLineSeparator(final List<TableHeader> blocks) {
    StringBuilder rowString = new StringBuilder();
    blocks.forEach(header -> rowString.append(buildColumn("", header.getColumnWidth(), "-", "+")));
    rowString.append("\n");
    return rowString.toString();
  }

  /*private void printRow (final List<TableColumn> blocks, final String filler, final String limiter) {
    StringBuilder rowString = new StringBuilder();
    blocks.forEach(header -> rowString.append(buildColumn(header.getValue(), header.getColumnWidth(), filler, limiter)));
    print(rowString.toString());
  }*/

  private String buildColumn(final String value, final int columnLength, final String filler, final String limiter) {
    int offSet = columnLength - value.length();
    String paddedString = fillString(value, filler ,offSet);
    return endString(paddedString , limiter);
  }

  private String fillString(final String originalString, final String filler, final int offSet) {
    StringBuilder paddedString = new StringBuilder(originalString);
    for(int index = 0; index < offSet; index++) {
      paddedString.append(filler);
    }
    return paddedString.toString();
  }

  private String endString (final String originalString, String limiter) {
    return originalString + limiter;
  }

  private void print(final String value) {
    System.out.println(value);
  }
}
