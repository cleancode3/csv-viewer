package com.schulung.csvviewer.tableviewer;

import com.schulung.csvviewer.tablecreator.Table;

public interface IPrintTable {
  String display(final Table table);
}
